/* Copyright (C) 2005  Ole André Vadla Ravnås <oleavr@gmail.com>
 * Copyright (C) 2009  Sjors Gielen <dazjorz@dazjorz.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef MIMICENCODE_H
#define MIMICENCODE_H

#include "mimic.h"
#include <QObject>

#define ENCODER_BUFFER_SIZE         16384
#define ENCODER_QUALITY_DEFAULT     0
#define ENCODER_QUALITY_MIN         0
#define ENCODER_QUALITY_MAX         10000

/**
 * @brief The encoder for the libmimic-qt library
 *
 * MimicEncode provides the API required for encoding MIMIC v2.x content.
 *
 * @{
 */
class MimicEncode : public Mimic
{
  Q_OBJECT

  public:
    MimicEncode();
    ~MimicEncode();

    void encode_main( uchar *out_buf, bool is_pframe);
    bool mimic_encoder_init(const MimicResEnum resolution);
    bool mimic_encode_frame(const uchar *input_buffer,
                                uchar *output_buffer,
                                int *output_length,
                                bool make_keyframe);

    int getBufferSize();
    void setQuality( int quality );

  private:
    double compare_blocks(const uchar *p1, const uchar *p2,
                              int stride, int row_count,
                              bool is_chrom);


    void _vlc_encode_block(const int *block, int num_coeffs);

};
/** @} */

#endif // MIMICENCODE_H

