/* Copyright (C) 2005  Ole André Vadla Ravnås <oleavr@gmail.com>
 * Copyright (C) 2009  Sjors Gielen <dazjorz@dazjorz.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef MIMIC_H
#define MIMIC_H

#include <QObject>

#define ENCODER_BUFFER_SIZE         16384
#define ENCODER_QUALITY_DEFAULT     0
#define ENCODER_QUALITY_MIN         0
#define ENCODER_QUALITY_MAX         10000

/**
 * @brief The Mimic common object
 *
 * Mimic is the base object for MimicEncode and MimicDecode.
 *
 * @{
 */
class Mimic : public QObject
{
  Q_OBJECT

  public:
    // The mimic encoding/decoding context returned by #mimic_open
    // and used for all further API calls until #mimic_close.
    typedef enum {
        MIMIC_RES_LOW,      /**< 160x120 resolution */
        MIMIC_RES_HIGH      /**< 320x240 resolution */
    } MimicResEnum;
    
    typedef struct {
        uchar length1;
        quint32 part1;

        uchar length2;
        quint32 part2;
    } VlcSymbol;

    typedef struct {
        quint32 magic;
        uchar pos_add;
        uchar num_bits;
    } VlcMagic;

    Mimic();
    virtual ~Mimic();

    bool isInitialized();
    int getWidth();
    int getHeight();
    int getQuality();
    int getFrameNum();

  protected:
    bool initialized_;

    int frame_width_;
    int frame_height_;
    int quality_;
    int num_coeffs_;

    int y_stride_;
    int y_row_count_;
    int y_size_;

    int crcb_stride_;
    int crcb_row_count_;
    int crcb_size_;

    int num_vblocks_y_;
    int num_hblocks_y_;

    int num_vblocks_cbcr_;
    int num_hblocks_cbcr_;

    uchar *cur_frame_buf_;
    uchar *prev_frame_buf_;

    char vlcdec_lookup_[2296];

    char *data_buffer_;
    uint data_index_;

    quint32 cur_chunk_;
    int cur_chunk_len_;

    quint32 *chunk_ptr_;
    bool read_odd_;

    int frame_num_;

    int ptr_index_;
    uchar *buf_ptrs_[16];

    void mimic_init(int width, int height);

    quint32 _read_bits(int num_bits);
    void _write_bits(quint32 bits, int length);

    void _fdct_quant_block(int *block, const uchar *src,
                           int stride, bool is_chrom, int num_coeffs);
    void _idct_dequant_block(int *block, bool is_chrom);

    VlcMagic *_find_magic(uint magic);
    void _initialize_vlcdec_lookup(char *lookup_tbl);

    void _rgb_to_yuv(const uchar *input_rgb,
                     uchar *output_y,
                     uchar *output_cb,
                     uchar *output_cr,
                     int width,
                     int height);
    void _yuv_to_rgb(const uchar *input_y,
                     const uchar *input_cb,
                     const uchar *input_cr,
                     uchar *output_rgb,
                     uint width,
                     uint height);

    void _deblock(uchar *blocks, uint stride, uint row_count);

    void deblock_horizontal( uchar *blocks, uint stride, uint row_count );
    void deblock_vertical( uchar *blocks, uint stride, uint row_count );

    bool deblock_h_consider_entire( uchar *blocks, uint stride );
    void deblock_h_do_entire( uchar *blocks, uint stride );
    void deblock_h_do_boundaries( uchar *blocks, uint stride );

    bool deblock_v_consider_entire( uchar *blocks, uint stride );
    void deblock_v_do_entire( uchar *blocks, uint stride );
    void deblock_v_do_boundaries( uchar *blocks, uint stride );
};
/** @} */

#endif // MIMIC_H

