/* Copyright (C) 2005  Ole André Vadla Ravnås <oleavr@gmail.com>
 * Copyright (C) 2009  Sjors Gielen <dazjorz@dazjorz.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "mimicencode.h"
#include <QtEndian>
#include <math.h>

extern uchar _col_zag[64];
extern Mimic::VlcSymbol _vlc_alphabet[16][128];

// need G_LN10 from the Gtk libraries... or is it in Qt too?
#define G_LN10  2.3025850929940456840179914546843642076011014886288
#define LUMINANCE_THRESHOLD   32.0f
#define CHROMINANCE_THRESHOLD 36.0f

MimicEncode::MimicEncode()
 : Mimic()
{
}

MimicEncode::~MimicEncode()
{
}

int MimicEncode::getBufferSize()
{
  if( !initialized_ )
    return -1;

  return ENCODER_BUFFER_SIZE;
}

void MimicEncode::setQuality( int quality )
{
  if( !initialized_ )
    return;

  if (quality < ENCODER_QUALITY_MIN || quality > ENCODER_QUALITY_MAX)
    return;

  quality_ = quality;
}

/**
 * Initialize the mimic encoder and prepare for encoding by
 * initializing internal state and allocating resources as
 * needed.
 * 
 * After initializing use #mimic_get_property to determine
 * the size of the output buffer needed for calls to
 * #mimic_encode_frame. Use #mimic_set_property to set
 * encoding quality.
 *
 * Note that once a given context has been initialized
 * for either encoding or decoding it is not possible
 * to initialize it again.
 *
 * @param resolution a #MimicResEnum used to specify the resolution
 * @returns true on success
 */
bool MimicEncode::mimic_encoder_init( const MimicResEnum resolution )
{
  int width, height;
    
  /* Check if we've been initialized before. */
  if( initialized_ )
  {
    return false;
  }

  /* Check resolution. */
  if (resolution == MIMIC_RES_LOW)
  {
    width = 160;
    height = 120;
  }
  else if (resolution == MIMIC_RES_HIGH)
  {
    width = 320;
    height = 240;
  }
  else
  {
    return false;
  }

  // Initialize!
  mimic_init(width, height);

  // Set a default quality setting.
  quality_ = ENCODER_QUALITY_DEFAULT;

  initialized_ = true;

  return true;
}

/**
 * Encode a MIMIC-encoded frame from RGB data.
 *
 * @param input_buffer buffer containing pixeldata in RGB 24-bpp packed pixel top-down format
 * @param output_buffer buffer that will receive the MIMIC-encoded frame
 *                      (use #mimic_get_property to determine the required buffer size)
 * @param output_length pointer to an integer that receives the length of the encoded data
 *                      written to output_buffer
 * @param make_keyframe whether the encoder should make this frame a keyframe
 * @returns #true on success
 */
bool MimicEncode::mimic_encode_frame( const uchar *input_buffer,
                                      uchar *output_buffer,
                                      int *output_length,
                                      bool make_keyframe )
{
    uchar *output_y, *output_cb, *output_cr;
    
    /*
     * Some sanity checks.
     */
    if ( input_buffer == NULL || output_buffer == NULL || output_length == NULL )
    {
        return false;
    }

    if ( ! initialized_ )
        return false;
    
    /*
     * Initialize state.
     */
    chunk_ptr_ = (quint32 *) (output_buffer + 20);
    cur_chunk_ = 0;
    cur_chunk_len_ = 0;
    
    if (frame_num_ == 0)
        make_keyframe = true;
    
    /*
     * Write header.
     */
    memset(output_buffer, 0, 20);
    *((quint16 *) (output_buffer +  0)) = qToLittleEndian(256);
    *((quint16 *) (output_buffer +  2)) = qToLittleEndian(quality_);
    *((quint16 *) (output_buffer +  4)) = qToLittleEndian(frame_width_);
    *((quint16 *) (output_buffer +  6)) = qToLittleEndian(frame_height_);
    *((quint32 *) (output_buffer + 12)) = qToLittleEndian((make_keyframe == 0));
    *(output_buffer + 16) = num_coeffs_;
    *(output_buffer + 17) = 0;
    
    /*
     * Perform RGB to YUV 420 conversion.
     */
    output_y  = cur_frame_buf_;
    output_cr = cur_frame_buf_ + y_size_;
    output_cb = cur_frame_buf_ + y_size_ + crcb_size_;

    _rgb_to_yuv(input_buffer,
                output_y,
                output_cb,
                output_cr,
                frame_width_,
                frame_height_);

    /*
     * Encode frame.
     */
    encode_main( output_buffer, (make_keyframe == false) );

    /*
     * Write out any pending bits to stream by zero-padding with 32 bits.
     */
    _write_bits( 0, 32 );

    /*
     * Calculate bytes written.
     */
    *output_length = (uchar *) chunk_ptr_ - output_buffer;
    
    /*
     * Increment frame counter.
     */
    frame_num_++;
    
    return true;
}


/*
 * encode_main
 *
 * Main encoding loop.
 */
void MimicEncode::encode_main( uchar *out_buf, bool is_pframe )
{
    unsigned int j;
    int x, y, i, offset, chrom_ch;
    int dct_block[64];
    uchar *src, *dst, *p1, *p2;
    double match;
    bool encoded;

    /*
     * Round down small differences in luminance channel.
     */
    if (is_pframe) {
        p1 = cur_frame_buf_;
        p2 = prev_frame_buf_;

        for (i = 0; i < y_size_; i++) {

            if (qAbs(p2[0] - p1[0]) < 7)
                p1[0] = p2[0];

            p1++;
            p2++;
        }
    }

    /*
     * Encode Y plane.
     */
    for (y = 0; y < num_vblocks_y_; y++) {

        for (x = 0; x < num_hblocks_y_; x++) {

            /* Calculate final offset into buffer. */
            offset = (y_stride_ * 8 * y) + (x * 8);

            src = NULL;
            encoded = false;

            if (is_pframe) {

                /* Is the current block similar enough to what it was in the previous frame? */

                match = compare_blocks(cur_frame_buf_ + offset,
                                       prev_frame_buf_ + offset,
                                       y_stride_, 8,
                                       false);

                if (match > LUMINANCE_THRESHOLD) {

                    /* Yes: write out '1' to indicate a no-change condition. */
                    
                    _write_bits( 1, 1 );

                    src = prev_frame_buf_ + offset;
                    encoded = true;

                } else {

                    /* No: Is the current block similar enough to what it was in one
                     *     of the (up to) 15 last frames preceding the previous? */

                    int best_index = 0;
                    double best_match = 0.0;

                    int num_backrefs = frame_num_ - 1;
                    if (num_backrefs > 15)
                        num_backrefs = 15;

                    for (i = 1; i <= num_backrefs; i++) {
                        
                        match = compare_blocks(buf_ptrs_[(ptr_index_ + i) % 16] + offset,
                                               cur_frame_buf_ + offset,
                                               y_stride_, 8,
                                               false);

                        if (match > LUMINANCE_THRESHOLD && match > best_match) {
                            best_index = i;
                            best_match = match;
                        }

                    }

                    if (best_index != 0) {

                        /* Yes: write out '01' to indicate a "change but like previous"-condition,
                         * followed by 4 bits containing the back-reference. */
                        _write_bits( 0, 1 );
                        _write_bits( 1, 1 );
                        _write_bits( best_index, 4 );

                        src = buf_ptrs_[(ptr_index_ + best_index) % 16] + offset;
                        encoded = true;

                    }
                }
            }

            if (!encoded) {

                /* Keyframe or in any case no? ;-) Well, encode it then. */

                if (is_pframe) {
                    _write_bits( 0, 1 );
                    _write_bits( 0, 1 );
                }

                _fdct_quant_block(dct_block,
                                  cur_frame_buf_ + offset,
                                  y_stride_,
                                  false,
                                  num_coeffs_);

                _vlc_encode_block(dct_block,
                                  num_coeffs_);

            }

            /* And if there was some kind of no-change condition,
             * we want to copy the previous block. */
            if (src != NULL) {
                
                dst = cur_frame_buf_ + offset;
                for (i = 0; i < 8; i++) {

                    memcpy(dst, src, 8);

                    src += y_stride_;
                    dst += y_stride_;
                }
                
            }

        }
        
    }

    /*
     * Encode Cr and Cb planes.
     */
    for (chrom_ch = 0; chrom_ch < 2; chrom_ch++) {

        /* Calculate base offset into buffer. */
        int base_offset = y_size_ + (crcb_size_ * chrom_ch);

        for (y = 0; y < num_vblocks_cbcr_; y++) {
            uchar tmp_block[64];
            uint num_rows = 8;
            
            /* The last row of blocks in chrominance for 160x120 resolution
             * is half the normal height and must be accounted for. */
            if (y + 1 == num_vblocks_cbcr_ && frame_height_ % 16 != 0)
                num_rows = 4;
            
            for (x = 0; x < num_hblocks_cbcr_; x++) {

                /* Calculate final offset into buffer. */
                offset = base_offset + (crcb_stride_ * 8 * y) + (x * 8);

                src = NULL;
                encoded = false;

                if (is_pframe) {
                    
                    /* Is the current block similar enough to what it was in the previous frame? */

                    match = compare_blocks(prev_frame_buf_ + offset,
                                           cur_frame_buf_ + offset,
                                           crcb_stride_, num_rows,
                                           true);

                    if (match > CHROMINANCE_THRESHOLD) {

                        /* Yes: write out '0' to indicate a no-change condition. */
                        
                        _write_bits( 0, 1 );
                        
                        encoded = true;

                        src = prev_frame_buf_ + offset;
                        dst = cur_frame_buf_ + offset;
                        for (j = 0; j < num_rows; j++) {

                            memcpy(dst, src, 8);

                            src += crcb_stride_;
                            dst += crcb_stride_;
                        }
                    }
                
                }

                if (!encoded) {

                    /* Keyframe or just not similar enough? ;-) Well, encode it then. */
                    
                    if (is_pframe)
                        _write_bits( 1, 1 );
                    
                    /* Use a temporary array to handle cases where the
                     * current block is not of normal height (see above). */
                    src = cur_frame_buf_ + offset;
                    dst = tmp_block;
                    for (j = 0; j < 8; j++) {
                        
                        memcpy(dst, src, 8);

                        if (j < (num_rows - 1))
                            src += crcb_stride_;
                        dst += 8;
                    }
                    
                    _fdct_quant_block(dct_block,
                                      tmp_block,
                                      8,
                                      true,
                                      num_coeffs_);

                    _vlc_encode_block(dct_block,
                                      num_coeffs_);
                    
                }

            }
            
        }
        
    }
    
    /*
     * Make a copy of the current frame and store in
     * the circular pointer list of 16 entries.
     */
    prev_frame_buf_ = buf_ptrs_[ptr_index_];
    memcpy(prev_frame_buf_, cur_frame_buf_,
           y_size_ + (crcb_size_ * 2));

    if (--ptr_index_ < 0)
        ptr_index_ = 15;
}

/*
 * compare_blocks
 *
 * Helper-function used to compare two blocks and
 * determine how similar they are.
 */
double MimicEncode::compare_blocks( const uchar *p1,
                                    const uchar *p2,
                                    int stride,
                                    int row_count,
                                    bool is_chrom)
{
    int i, j, sum;
    double d;

    sum = 0;

    for (i = 0; i < row_count; i++) {

        for (j = 0; j < 8; j++) {

            int d = p2[j] - p1[j];

            sum += d * d;
        }

        p1 += stride;
        p2 += stride;
    }

    if (is_chrom) {
        if (row_count == 8)
            d = sum * 0.015625;
        else
            d = sum * 0.03125;
    } else {
        d = sum / 64;
    }
    
    if (d == 0.0f)
        return 100.0f;
    else
        return (10.0f * log(65025.0f / d)) / G_LN10;
}

/*
 * _vlc_encode_block
 *
 * Serialize an 8x8 block using variable length coding.
 */
void MimicEncode::_vlc_encode_block( const int *block, int num_coeffs )
{
    int i, num_zeroes;

    /* The DC value is written out as is. */
    _write_bits( block[0], 8 );

    /* Number of zeroes prefixing the next non-zero value. */
    num_zeroes = 0;

    for (i = 1; i < num_coeffs && num_zeroes <= 14; i++) {

        /* Fetch AC coefficients from block in zig-zag order. */
        int value = block[_col_zag[i]];

        if (value != 0) {
            VlcSymbol sym;

            /* Clip input values to [-128, +128]. */
            if (value < -128)
                value = -128;
            else if (value > 128)
                value = 128;

            /* Look up symbol for the current non-zero value. */
            sym = _vlc_alphabet[num_zeroes][ qAbs(value) - 1 ];

            /* No symbol? very rare... */
            if (sym.length1 <= 0)
                break;

            /* The symbols for negative values are the same as for positives, minus one. */
            if (value < 0) {
                if (sym.length2 > 0)
                    sym.part2 -= 1;
                else
                    sym.part1 -= 1;
            }

            /* Write out the full symbol. */
            _write_bits( sym.part1, sym.length1 );
            if (sym.length2 > 0)
                _write_bits( sym.part2, sym.length2 );

            /* Start counting zeroes again. */
            num_zeroes = 0;
        } else {
            num_zeroes++;
        }
    }

    /* Write out EOB if necessary. */
    if (num_zeroes > 0)
        _write_bits( 0xA, 4 );
}

