/* Copyright (C) 2005  Ole André Vadla Ravnås <oleavr@gmail.com>
 * Copyright (C) 2009  Sjors Gielen <dazjorz@dazjorz.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef MIMICDECODE_H
#define MIMICDECODE_H

#include "mimic.h"
#include <QObject>


/**
 * @brief The decoder for the libmimic-qt library
 *
 * MimicDecode provides the API required for decoding MIMIC v2.x content.
 *
 * @{
 */
class MimicDecode : public Mimic
{
  Q_OBJECT

  public:
    MimicDecode();
    ~MimicDecode();

    bool          initDecoder( const QByteArray& frame );
    bool          initDecoder( const uchar *frame_buffer );
    QByteArray    decodeFrame( const QByteArray& input );
    bool          decodeFrame( const uchar *input_buffer, uchar *output_buffer );

    int           getBufferSize();

  private:
    bool          startDecode( bool is_pframe );
    bool          _vlc_decode_block(int *block, int num_coeffs);
};
/** @} */

#endif // MIMICDECODE_H
