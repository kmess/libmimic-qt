/* Copyright (C) 2005  Ole André Vadla Ravnås <oleavr@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "mimic.h"

#include <QtEndian>

/*
 * _read_bits
 *
 * Internal helper-function used to read num_bits
 * from stream.
 */
quint32 Mimic::_read_bits(int num_bits)
{
  quint32 bits;

  if (cur_chunk_len_ >= 16)
  {
    uchar *input_buf = (uchar *) data_buffer_ + data_index_;

    if (!read_odd_)
    {
      read_odd_ = true;

      cur_chunk_ = (input_buf[3] << 24) |
                   (input_buf[2] << 16) |
                   (input_buf[1] <<  8) |
                    input_buf[0];

    }
    else
    {
      read_odd_ = false;

      cur_chunk_ = (input_buf[1] << 24) |
                   (input_buf[0] << 16) |
                   (input_buf[7] <<  8) |
                    input_buf[6];

      data_index_ += 4;
    }

    cur_chunk_len_ -= 16;
  }

  bits = (cur_chunk_ << cur_chunk_len_) >> (32 - num_bits);
  cur_chunk_len_ += num_bits;

  return bits;
}

/*
 * _write_bits
 *
 * Internal helper-function used to write "length"
 * bits of "bits" to stream.
 */
void Mimic::_write_bits(quint32 bits, int length)
{
    /* Left-align the bit string within its 32-bit container. */
    bits <<= (32 - length);

    /* Append the bit string (one or more of the trailing bits might not fit, but that's ok). */
    cur_chunk_ |= bits >> cur_chunk_len_;
    cur_chunk_len_ += length;

    /* Is it full? */
    if (cur_chunk_len_ >= 32) {

        /* Add the full 32-bit chunk to the stream and update counter. */
        chunk_ptr_[0] = qToLittleEndian( cur_chunk_ );
        chunk_ptr_++;
        cur_chunk_len_ -= 32;

        /* Add any trailing bits that didn't fit. */
        cur_chunk_ = bits << (length - cur_chunk_len_);
    }
}

