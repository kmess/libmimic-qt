/* Copyright (C) 2005  Ole André Vadla Ravnås <oleavr@gmail.com>
 * Copyright (C) 2009  Sjors Gielen <dazjorz@dazjorz.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "mimic.h"
#include <stdlib.h>
#include <QDebug>
#include <QtEndian>

/**
 * Constructor. Does not call #mimic_encoder_init or #mimic_decoder_init.
 */
Mimic::Mimic()
 : QObject(),
  initialized_(false)
{
}

/**
 * Destructor.
 */
Mimic::~Mimic()
{
  if( initialized_ )
  {
    int i;

    free( cur_frame_buf_ );

    for (i = 0; i < 16; i++)
    {
      free( buf_ptrs_[i] );
    }
  }
}

/*
 * mimic_init
 *
 * Internal helper-function used to initialize
 * a given context.
 */
void Mimic::mimic_init( int width, int height )
{
  int bufsize, i;
    
  // Dimentions-related.
  frame_width_      = width;
  frame_height_     = height;

  y_stride_         = frame_width_;
  y_row_count_      = frame_height_;
  y_size_           = y_stride_ * y_row_count_;
    
  crcb_stride_      = y_stride_     / 2;
  crcb_row_count_   = y_row_count_  / 2;
  crcb_size_        = crcb_stride_ * crcb_row_count_;
    
  num_vblocks_y_    = frame_height_ / 8;
  num_hblocks_y_    = frame_width_  / 8;

  num_vblocks_cbcr_ = frame_height_ / 16;
  num_hblocks_cbcr_ = frame_width_  / 16;

  if (frame_height_ % 16 != 0)
  {
    num_vblocks_cbcr_++;
  }

  // Initialize state.
  frame_num_  = 0;
  ptr_index_  = 15;
  num_coeffs_ = 28;

  // Allocate memory for buffers.
  cur_frame_buf_ = (uchar*) malloc( ( (320 * 240 * 3) / 2 ) * sizeof(uchar) );

  bufsize = y_size_ + (crcb_size_ * 2);
  for (i = 0; i < 16; i++)
  {
    buf_ptrs_[i] = (uchar*) malloc( bufsize * sizeof(uchar) );
  }

  // Initialize vlc lookup used by decoder.
  _initialize_vlcdec_lookup(vlcdec_lookup_);
}

bool Mimic::isInitialized()
{
  return initialized_;
}

int Mimic::getWidth()
{
  if( ! initialized_ )
    return -1;

  return frame_width_;
}

int Mimic::getHeight()
{
  if( ! initialized_ )
    return -1;

  return frame_height_;
}

int Mimic::getQuality()
{
  if( ! initialized_ )
    return -1;

  return quality_;
}

int Mimic::getFrameNum()
{
  return frame_num_;
}
