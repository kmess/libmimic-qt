#include "../src/mimicdecode.h"
#include <KApplication>
#include <KDebug>
#include <QFile>
#include <QPixmap>
#include <QLabel>

class TestApp : public KApplication
{
  Q_OBJECT

  public:
    TestApp();
    ~TestApp();

  private slots:
    void decodeFrame();

  private:
    void newFrame();
    int i_;
    bool initialized_;
    QFile dump_;
    QPixmap pixmap_;
    QLabel label_;
    MimicDecode mimic_;
};
